(setq gnus-select-method '(nntp "news.epita.fr"))

(setq user-full-name "Jean-Denis Marie-Sainte")
(setq user-mail-adress "mare-_j@epitech.net")

(setq gnus-signature-separator
           '("^-- $"))

(setq gnus-posting-styles
          '(
	    ((message-news-p)
	     (name "Jean-Denis Marie-Sainte")
             (address "marie-_j@epitech.net")
             (organization "something")
             (signature-file "~/.signature"))
	    )
	  )

(provide 'gnus-conf)
