(deftheme marie-_j
  "Created 2015-05-22.")

(custom-theme-set-variables
 'marie-_j
 '(inhibit-startup-screen t)
 '(normal-erase-is-backspacenormal-erase-is-backspace-mode 1)
 '(package-archives (quote (("marmalade" . "http://marmalade-repo.org/packages/") ("gnu" . "http://elpa.gnu.org/packages/") ("melpa" . "http://stable.melpa.org/packages/"))))
 '(scroll-bar-mode nil)
 '(size-indication-mode t)
 '(show-paren-mode t)
 '(global-auto-complete-mode t)
 '(fringe-mode 0)
 '(display-time-mode t)
 '(cua-mode t)
 '(custom-safe-themes (quote ("fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" default))))

(provide-theme 'marie-_j)
