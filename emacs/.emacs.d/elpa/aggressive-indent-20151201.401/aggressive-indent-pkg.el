(define-package "aggressive-indent" "20151201.401" "Minor mode to aggressively keep your code always indented" (quote ((emacs "24.1") (cl-lib "0.5"))))
