;;; cppcheck-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "cppcheck" "cppcheck.el" (22129 19391 508819
;;;;;;  607000))
;;; Generated autoloads from cppcheck.el

(autoload 'cppcheck "cppcheck" "\


\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; cppcheck-autoloads.el ends here
