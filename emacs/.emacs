;;; package  --- Summary
;;; Commentary:

;;; Code:
(eval-after-load "warnings"
  '(setq display-warning-minimum-level :error))
;;(load "~/.emacs.d/load/std.elc")			;; Epitech classic header
;;(load "~/.emacs.d/load/std_comment.elc")		;; Epitech classic header

(add-to-list 'load-path "~/.emacs.d/load")

(load "~/.emacs.d/load/load_d_p")
(load "~/.emacs.d/load/require_pk")
(load "~/.emacs.d/load/key_bdg")
(load "~/.emacs.d/load/conf_pkg")
;; nb de colonne
(load "~/.emacs.d/load/use_f")
(setq user-mail-address "marie-_j@epitech.eu")
(if (file-exists-p "~/.emacs.d/load/std.el")
    (load "std.el"))
(if (file-exists-p "~/.emacs.d/load/std_comment.el")
    (load "std_comment.el"))

(custom-set-variables
 '(color-theme-directory (quote ("~/.emacs.d/elpa/solarized-theme-20150627.1412")))
 '(column-number-mode t)
 '(cua-mode t nil (cua-base))
 '(custom-safe-themes (quote (default)))
 '(display-time-mode t)
 '(fringe-mode 0 nil (fringe))
 '(global-auto-complete-mode t)
 '(inhibit-startup-screen t)
 '(max-mini-window-height nil)
 '(minibuffer-auto-raise t)
 '(minibuffer-depth-indicate-mode t)
 '(normal-erase-is-backspacenormal-erase-is-backspace-mode 1 t)
 '(package-archives (quote (("gnu" . "http://elpa.gnu.org/packages/") ("melpa" . "http://stable.melpa.org/packages/") ("marmalade" . "http://marmalade-repo.org/packages/"))))
 '(pong-timer-delay 0.02)
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(size-indication-mode t))


(defun refresh-file ()
  (interactive)
  (revert-buffer t t t)
  (load-file "~/.emacs")
  )
(setq ido-ignore-buffers (quote ("\\`\\*breakpoints of.*\\*\\'"
				 "\\`\\*stack frames of.*\\*\\'" "\\`\\*gud\\*\\'"
				 "\\`\\*locals of.*\\*\\'"  "\\` ")))
;;   (interactive))
