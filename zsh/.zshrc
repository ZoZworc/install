# Configuration for zsh (.zshrc)

case ${OSTYPE} in
    *netbsd*)
	FLAG=1
	;;
    *freebsd*|*darwin*)
	FLAG=2
	;;
    *openbsd*)
	FLAG=3
	;;
    *linux-gnu*)
	FLAG=4
	;;
    *solaris*)
	FLAG=5
	;;
esac

export EDITOR='emacs -nw'
export HISTFILE="${HOME}/.history"
export SAVEHIST='1000'
export LOGCHECK='60'
export MAILCHECK='0'
export PAGER='less'
export SVN_EDITOR='emacs -nw'
export WATCH='all'
export WATCHFMT="%n has %a %l from %m at %T"

######################################################################
##
## Changing Directories
##
######################################################################

# If a command  is issued that can't be executed  as a normal command,
# and the command  is the name of a directory,  perform the cd command
# to that directory.
setopt AUTO_CD

######################################################################
##
## Completion
##
######################################################################

# If unset, key  functions that list completions try  to return to the
# last prompt if given a  numeric argument. If set these functions try
# to return to the last prompt if given no numeric argument.
setopt ALWAYS_LAST_PROMPT

# If a  completion is performed with  the cursor within a  word, and a
# full completion is  inserted, the cursor is moved to  the end of the
# word.  That is, the cursor is moved to the end of the word if either
# a single match is inserted or menu completion is performed.
setopt ALWAYS_TO_END

# Automatically list choices on an ambiguous completion.
setopt AUTO_LIST

# Automatically  use  menu  completion  after the  second  consecutive
# request  for  completion,  for  example  by  pressing  the  tab  key
# repeatedly. This option is overridden by MENU_COMPLETE.
setopt AUTO_MENU

# If  a  parameter  is  completed  whose  content is  the  name  of  a
# directory, then add a trailing slash instead of a space.
setopt AUTO_PARAM_SLASH

# Prevents  aliases   on  the  command  line   from  being  internally
# substituted before  completion is attempted.  The effect  is to make
# the alias a distinct command for completion purposes.
setopt COMPLETE_ALIASES

# If unset, the cursor is set to  the end of the word if completion is
# started. Otherwise it  stays there and completion is  done from both
# ends.
setopt COMPLETE_IN_WORD

# Whenever  a command completion  is attempted,  make sure  the entire
# command  path is  hashed  first.  This  makes  the first  completion
# slower.
setopt HASH_LIST_ALL

# This option works when AUTO_LIST  or BASH_AUTO_LIST is also set.  If
# there is an  unambiguous prefix to insert on  the command line, that
# is done without  a completion list being displayed;  in other words,
# auto-listing  behaviour  only  takes  place when  nothing  would  be
# inserted.  In the  case of BASH_AUTO_LIST, this means  that the list
# will be delayed to the third call of the function.
setopt LIST_AMBIGUOUS

# Beep on  an ambiguous completion.  More accurately,  this forces the
# completion widgets to return status  1 on an ambiguous com- pletion,
# which causes the shell to beep  if the option BEEP is also set; this
# may be modified if completion is called from a user-defined widget.
unsetopt LIST_BEEP

# When listing files  that are possible completions, show  the type of
# each file with a trailing identifying mark.
setopt LIST_TYPES

######################################################################
##
## History
##
######################################################################

# If this is  set, zsh sessions will append their  history list to the
# history file,  rather than replace  it. Thus, multiple  parallel zsh
# sessions  will all  have the  new entries  from their  history lists
# added to  the history file, in  the order that they  exit.  The file
# will still be periodically re-written  to trim it when the number of
# lines grows  20% beyond the  value specified by $SAVEHIST  (see also
# the HIST_SAVE_BY_COPY option).
setopt APPEND_HISTORY

# Save each  command's beginning timestamp (in seconds  since the epoch)
# and the duration (in seconds) to the history file.
setopt EXTENDED_HISTORY

# Beep when an  attempt is made to access a  history entry which isn't
# there.
unsetopt HIST_BEEP

# When  searching for  history  entries  in the  line  editor, do  not
# display  duplicates  of  a   line  previously  found,  even  if  the
# duplicates are not contiguous.
setopt HIST_FIND_NO_DUPS

# Do  not  enter command  lines  into the  history  list  if they  are
# duplicates of the previous event.
setopt HIST_IGNORE_DUPS

# Remove  superfluous blanks from each command line being added to
# the history list.
setopt HIST_REDUCE_BLANKS

# This options works like APPEND_HISTORY except that new history lines
# are  added to  the  $HISTFILE  incrementally (as  soon  as they  are
# entered), rather than waiting until  the shell exits.  The file will
# still be periodically re-written to trim it when the number of lines
# grows  20% beyond  the value  specified by  $SAVEHIST (see  also the
# HIST_SAVE_BY_COPY option).
setopt INC_APPEND_HISTORY

if [ ${FLAG} -eq 1 ] || [ ${FLAG} -eq 2 ] || [ ${FLAG} -eq 4 ]
then
    alias cp="nocorrect cp -v"
    alias mv="nocorrect mv -v"
    alias rm="nocorrect rm -v"
fi

# remove scratch and unwanted files
clean()
{
    SEARCH='.'
    if [ ${1} ]
    then
	SEARCH=${1}
    fi

    if [ ${FLAG} -eq 1 ] || [ ${FLAG} -eq 2 ] || [ ${FLAG} -eq 4 ]
    then
	find ${SEARCH} \( -name "*~" -or -name ".*~" -or -name "#*#" \) -exec rm -frv {} \;
    else
	find ${SEARCH} \( -name "*~" -or -name ".*~" -or -name "#*#" \) -exec rm -fr {} \;
    fi
}

######################################################################
###
### Prompt system for school and home compatibility.
###
######################################################################
setprompt()
{
    # C-q Esc
    CL_NORMAL='%{[0m%}'	# %{[0m%} = normal (or %{"$'\e[0m'"%})
    CL_GREY='%{[0;30m%}'	# %{[0;30m%} = grey
    CL_RED='%{[0;31m%}'	# %{[0;31m%} = red
    CL_GREEN='%{[0;32m%}'	# %{[0;32m%} = green
    CL_YELLOW='%{[0;33m%}'	# %{[0;33m%} = yellow
    CL_BLUE='%{[0;34m%}'	# %{[0;34m%} = blue
    CL_VIOLET='%{[0;35m%}'	# %{[0;35m%} = violet
    CL_CYAN='%{[0;36m%}'	# %{[0;36m%} = cyan
    CL_BOLD_GREY='%{[1;30m%}'	# %{[1;30m%} = bold grey
    CL_BOLD_RED='%{[1;31m%}'	# %{[0;31m%} = bold red
    if [ ${USER} = 'root' ]
    then
	PR_USER="[${CL_RED}%n${CL_NORMAL}"
	PR_HOST="${CL_CYAN}%m${CL_NORMAL}]%# "
    else
	PR_USER="[${CL_YELLOW}%n${CL_NORMAL}"
	PR_HOST="${CL_CYAN}%m${CL_NORMAL}]%# "
    fi
    PROMPT="${PR_USER}@${PR_HOST}"
    RPROMPT="(%y) [${CL_CYAN}%~${CL_NORMAL}]"
}
setprompt

export LD_LIBRARY_PATH="/home/boufid_n/.froot/lib/"
export C_INCLUDE_PATH="/home/boufid_n/.froot/include/"
export CPLUS_INCLUDE_PATH="/home/boufid_n/.froot/include/"

## ALIASES #####################################################################

export PATH=$PATH":$HOME/bin"

alias mr_clean='find . -name "*~" -delete -o -name "#*#" -delete'

alias ..='cd ..'

alias emacs='emacs -nw'
alias ne='emacs'

alias m='make'
alias ma='make all'
alias mf='make fclean'
alias mr='make re'
alias mc='make clean'

alias ls='ls --color'
alias ll='ls -l'
alias la='ls -la'
alias aspi='wget -r -k -E -np'
alias lt='tree'

alias ping='ping -c 3'
alias python='python3.5'

alias g='g++ *.cpp -W -Wall -Wextra -Werror'

alias myaliases='ne ~/.zshrc'
alias resource='source ~/.zshrc'

alias dl='youtube-dl -x'
alias mytar='tar cvf koalinette.tar main attendu test mini-koalinette.sh'

alias up='sudo apt update ; sudo apt upgrade ; sudo apt-get autoremove'
alias gba='gambatte_sdl -v 5 --resampler 5'
## END OF ALIASES ##############################################################
