#!/bin/bash

RED="\033[31m"
DEFAULT="\033[0m"
GREEN="\033[32m"

Y="Y"
ZSH=0
EMACS=0
SSH=0
RUBY=0
RET= 2> /dev/null > /dev/null

aff()
{
    echo -e $1 $2 $3 $DEFAULT
}

inst_add()
{
    PROG=$1
    PRESS=$2
    DEPO=$3

    if [ "$PRESS" == "$Y" ]
    then
	tmp="Y"
    else
	aff "-n" $GREEN "Installation de $PROG [Y/n]:";read tmp
    fi
    if [ "$tmp" == "$Y" ]
    then
	add-apt-repository "$DEPO" 2> /dev/null > /dev/null &&
	    apt-get update 2> /dev/null > /dev/null &&
	    echo "Y" | apt-get install $PROG 2> /dev/null > /dev/null
	[ $? -eq 0 ] && aff "" $GREEN "[OK] $PROG" || aff "" $RED "[XX] $PROG"
    fi
}

inst()
{
    PROG=$1
    PRESS=$2

    if [ "$PRESS" == "$Y" ]
    then
	tmp="Y"
    else
	aff "-n" $GREEN "Installation de $PROG [Y/n]:";read tmp
    fi

    if [ "$tmp" == "$Y" ]
    then
	echo "Y" | apt-get install $PROG 2> /dev/null > /dev/null
	[ $? -eq 0 ] && aff "" $GREEN "[OK] $PROG" || aff "" $RED "[XX] $PROG"
	case "$PROG" in
	    "zsh")
		ZSH=1
		;;
	    "emacs")
		EMACS=1
		;;
	    "ssh")
		SSH=1
		;;
	    "ruby-full")
		RUBY=1
		;;
	esac
    fi
}

update()
{
    aff "" $GREEN "Mise à Jour : liste des paquets"
    apt-get update 2> /dev/null > /dev/null
    aff "" $GREEN "Mise à Jour: paquets"
    echo "Y" | apt-get upgrade 2> /dev/null > /dev/null
}

installation()
{
    echo -e $RED "Script d'installation ubuntu 16.04" $DEFAULT
    update
    aff "-n" $RED "Tout installer [Y/n] :";read tmp
    inst "zsh" $tmp
    inst "emacs" $tmp
    inst "git" $tmp
    inst "terminator" $tmp
    inst "g++" $tmp
    inst "php" $tmp
    inst "valgrind" $tmp
    inst "ssh" $tmp
    inst "wget" $tmp
    inst "tree" $tmp
    inst "vim" $tmp
    inst "htop" $tmp
    inst "vlc" $tmp
    inst "most" $tmp
    inst "glibc-doc" $tmp
    inst "nasm" $tmp
    inst "chromium-browser" $tmp
    inst "flashplugin-installer" $tmp
    inst "default-jre" $tmp
    inst "ruby-full" $tmp
    inst "soundconverter" $tmp
    inst "openssh-server" $tmp
    inst_add "sublime-text" $tmp "ppa:webupd8team/sublime-text-2"
    inst_add "skype" $tmp "deb http://archive.canonical.com/ubuntu $(lsb_release -sc) partner"

    inst "gimp" $tmp
    inst "youtube-dl" $tmp

    echo
    aff "" $RED "Configuration :"
    if [ $ZSH -eq 1 ]
    then
	aff "" $GREEN "Préparation zsh"
	cp zsh/.zshrc ~/ 2> /dev/null > /dev/null
	cp zsh/.zshrc /home/$1/ 2> /dev/null > /dev/null
	echo "/bin/zsh" | chsh root
	echo "/bin/zsh" | chsh exploit
    fi
    
    if [ $EMACS -eq 1 ]
    then
	aff "-n" $GREEN "Conf emacs de batard ? [Y/n]:"; read $tmp
	if [ "$tmp" == "Y" ]
	then
	    cp -r emacs/.emacs* /home/$1/ 2> /dev/null > /dev/null
	    chown exploit /home/$1/.emacs* -R 2> /dev/null > /dev/null
	    chgrp exploit /home/$1/.emacs* -R 2> /dev/null > /dev/null
	fi
    fi

    if [ $SSH -eq 1 ]
    then
	aff "" $GREEN "Préparation ssh"
	mkdir /home/$1/.ssh 2> /dev/null > /dev/null
	cp ssh/.ssh/* /home/$1/.ssh/  2> /dev/null > /dev/null
	echo "" > /home/$1/.ssh/authorized_keys
	chown $1 /home/$1/.ssh/*
	chgrp $1 /home/$1/.ssh/*
    fi

    aff "-n" $GREEN "Est tu nicolas ? [Y/n]:"; read $tmp
    if [ "$tmp" == "Y" ]
    then
	cp -r perso/bin /home/$1/ 2> /dev/null > /dev/null
	chown exploit /home/$1/bin -R 2> /dev/null > /dev/null
	chgrp exploit /home/$1/bin -R 2> /dev/null > /dev/null
    fi
}

if [ $# -ne 1 ]
then
    	aff "" $RED "$0: $0 -h for help"
    	exit 1
fi

if [ "$1" == "-h" ]
then
    aff "" $GREEN "$0 : usage :"
    aff "" $GREEN "     -> $0 -u : for update"
    aff "" $GREEN "     -> $0 [user name] : for installation for [user name]"
    exit 1
fi

if [ "$UID" -ne "0" ]
then
    aff "" $RED "Vous devez être root !!!"
    exit 1
fi

case $1 in
    "-u")
	update
	;;
    *)
	installation $1
	;;
esac
exit 0
